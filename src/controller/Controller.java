package controller;

import api.ITaxiTripsManager;
import model.data_structures.MiLista;
import model.logic.TaxiTripsManager;
import model.vo.Service;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	private static ITaxiTripsManager  manager = new TaxiTripsManager();
	
	public static void loadServices() {
		// To define the dataset file's name 
		String serviceFile = "./data/taxi-trips-wrvz-psew-subset-medium.json";
		
		manager.loadServices( serviceFile);
	}
	
	public static MiLista<Service> serviciosEnArea(String pArea)
	{
		return manager.serviciosEnArea(pArea);
	}

	public static MiLista<Service> agruparServiciosXDistancia(double pDistancia)
	{
		return manager.agruparServiciosXDistancia(pDistancia);
	}
}
