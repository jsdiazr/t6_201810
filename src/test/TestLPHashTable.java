package test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import model.data_structures.LPHashTable;
import model.data_structures.MiLista;

public class TestLPHashTable 
{
	public LPHashTable<Integer,Integer> setUpScenario1()
	{
		LPHashTable<Integer,Integer> table = new LPHashTable<Integer,Integer>();
		return table;
	}
	
	public LPHashTable<Integer,Integer> setUpScenario2()
	{
		LPHashTable<Integer,Integer> table = new LPHashTable<Integer,Integer>();
		
		table.put(-10,-10);
		table.put(-5,-5);
		table.put(0,0);
		table.put(10,10);
		table.put(45,45);
		table.put(1000,1000);
		
		return table;
	}
	
	@Test
	public void testPut()
	{
		LPHashTable<Integer,Integer> table = setUpScenario1();
		table.put(11,11);
		
		Integer val = table.get(11);
		assertTrue(val == 11);
		assertTrue(table.getN() == 1);
	}
	
	@Test
	public void testGet()
	{
		LPHashTable<Integer,Integer> table = setUpScenario2();
		
		assertTrue(table.get(-10) == -10);
		assertTrue(table.get(-5) == -5);
		assertTrue(table.get(0) == 0);
		assertTrue(table.get(10) == 10);
		assertTrue(table.get(45) == 45);
		assertTrue(table.get(1000) == 1000);
	}
	
	@Test
	public void testDelete()
	{
		LPHashTable<Integer,Integer> table = setUpScenario2();
		
		int val = table.delete(10);
		
		assertTrue(val == 10);
		assertTrue(table.get(10) == null);
		
	}
	
	@Test
	public void testKeys()
	{
		LPHashTable<Integer,Integer> table = setUpScenario2();
		
		MiLista<Integer> lista = (MiLista<Integer>) table.keys();
		
		assertTrue(lista.darTamanio() == 6);
		
	}
	
	@Test
	public void testRehash()
	{
		LPHashTable<Integer,Integer> table = setUpScenario2();
		
		int s1 = table.getM();
		int h1 = table.hash(45);
		table.rehash();
		int s2 = table.getM();
		int h2 = table.hash(45);
		
		assertTrue(s1 < s2);
		assertTrue(h1 != h2);
	}
	
}
