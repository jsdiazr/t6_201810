package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.*;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import api.ITaxiTripsManager;
import model.data_structures.MergeSort;
import model.data_structures.MiLista;
import model.data_structures.SCHashTable;
import model.vo.Service;

public class TaxiTripsManager implements ITaxiTripsManager {
	
	private SCHashTable<String,MiLista<Service>> req1 = new SCHashTable<String,MiLista<Service>>();
	
	private SCHashTable <Double, MiLista <Service>> req2 = new SCHashTable<Double, MiLista<Service>>();
	
	public void loadServices(String serviceFile) {
		// TODO Auto-generated method stub
		System.out.println("Inside loadServices with File:" + serviceFile);
		int contador = 0;
		
		JsonParser parser = new JsonParser();
		
		try {
			String jsonDataList = "./data/taxi-trips-wrvz-psew-subset-medium.json";
			
			JsonArray array = (JsonArray) parser.parse(new FileReader(jsonDataList));
			
			for (int i = 0; array != null && i < array.size(); i++)
			{
				JsonObject obj= (JsonObject) array.get(i);
				
				String pickup_community_area = "NaN";
				if( obj.get("pickup_community_area") != null)
				{ pickup_community_area = obj.get("pickup_community_area").getAsString(); }
				
				String trip_end_timestamp = "NaN";
				if( obj.get("trip_end_timestamp") != null)
				{ trip_end_timestamp = obj.get("trip_end_timestamp").getAsString(); }
				
				String trip_id = "NaN";
				if( obj.get("trip_id") != null)
				{ trip_id = obj.get("trip_id").getAsString(); }
				
				String trip_miles = "NaN";
				if( obj.get("trip_miles") != null)
				{ trip_miles = obj.get("trip_miles").getAsString(); }
				
				String trip_seconds = "NaN";
				if( obj.get("trip_seconds") != null)
				{ trip_seconds = obj.get("trip_seconds").getAsString(); }
				
				String trip_start_timestamp = "NaN";
				if( obj.get("trip_start_timestamp") != null)
				{ trip_start_timestamp = obj.get("trip_start_timestamp").getAsString(); }
				
				if(pickup_community_area != "NaN")
				{
					if(req1.get(pickup_community_area) == null)
					{
						MiLista<Service> sL = new MiLista<Service>();
						sL.agregarElemento(new Service(trip_id, trip_seconds, trip_miles, pickup_community_area, trip_start_timestamp, trip_end_timestamp));
						req1.put(pickup_community_area, sL);
					}
					else
					{
						MiLista<Service> sL = req1.get(pickup_community_area);
						sL.agregarElemento(new Service(trip_id, trip_seconds, trip_miles, pickup_community_area, trip_start_timestamp, trip_end_timestamp));
						req1.put(pickup_community_area, sL);
					}
				}
				
				if (!trip_miles.equals("NaN"))
				{
					if (req2.get(Math.ceil(Double.parseDouble(trip_miles))) == null)
					{
					MiLista<Service> n = new MiLista<Service>();
					n.agregarElemento(new Service(trip_id, trip_seconds, trip_miles, pickup_community_area, trip_start_timestamp, trip_end_timestamp));
					req2.put(Math.ceil(Double.parseDouble(trip_miles)), n);
					}
					
					else
					{
						req2.get(Math.ceil(Double.parseDouble(trip_miles))).agregarElemento(new Service(trip_id, trip_seconds, trip_miles, pickup_community_area, trip_start_timestamp, trip_end_timestamp));
					}
				}
				
				
				contador ++;
				System.out.println(contador + ""); 
				
			}
		}
		catch (JsonIOException e1 ) {
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) {
			e3.printStackTrace();
		}
		catch (Exception e4)
		{
			e4.printStackTrace();
		}

	}

	@Override
	public MiLista<Service> serviciosEnArea(String pArea) 
	{
		MiLista<Service> lS = req1.get(pArea);
		
		Service[] s = new Service[lS.darTamanio()];
		
		for(int i = 0; i < lS.darTamanio(); i++)
		{
			s[i] = lS.darElementoPosicion(i);
		}
		
		MergeSort.sort(s);
		
		System.out.println("Servicios en el �rea " + pArea + ":");
		System.out.println("-------------------------------------------------------");
		
		for(int i = 0; i < lS.darTamanio(); i++)
		{
			Service actual = lS.darElementoPosicion(i);
			System.out.println(actual.getTripId() + " lleg� en: " + actual.getTripEndTimeStamp());
		}
		
		return lS;
		
	}

	@Override
	public MiLista<Service> agruparServiciosXDistancia(double pDistancia)
	{
		double i = Math.ceil(pDistancia);
		MiLista<Service> l = req2.get(i);
		Service [] s = new Service [l.darTamanio()];
		
		for(int x = 0; x < l.darTamanio(); x++)
		{
			s[x] = l.darElementoPosicion(x);
		}
		
		System.out.println("Servicios con distancia menor o igual a " + pDistancia + ":");
		System.out.println("-------------------------------------------------------");
		
		for(int j = 0; j < l.darTamanio(); j++)
		{
			Service actual = l.darElementoPosicion(j);
			System.out.println("El taxi " + actual.getTripId() + " recorri� " + actual.getTripMiles());
		}
		
		return l;
		
	}
	
	


}
