package model.data_structures;

public interface IHashTable<K,V> {
	
	
	/*
	 * Agregar una dupla (K, V) a la tabla. 
	 * Si la llave K existe, se reemplaza su valor V asociado. V no puede ser null.
	 */
	public void put(K pK, V pV);
	
	/*
	 * Obtener el valor V asociado a la llave K. V no puede ser null.
	 */
	public V get(K pK);
	
	/*
	 * Borrar la dupla asociada a la llave K. 
	 * Se obtiene el valor V asociado a la llave K. Se obtiene null si la llave K no existe.
	 */
	public V delete(K pK);
	
	/*
	 * Conjunto de llaves K presentes en la tabla.
	 */
	public Iterable<K> keys();
	
	/*
	 * Obtener un int entre 0 y M-1 para usar como �ndice en un array.  
	 */
	public int hash(K pK);

	/*
	 * Cambia el tama�o de la tabla
	 */
	public void rehash();

	
	
	
}
