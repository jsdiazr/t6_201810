package model.data_structures;

public class SCHashTable<K,V> implements IHashTable<K,V>{

	public static final int CAPACIDAD_INICIAL = 13;
	
	private int N;
	private int M;
	private static final double FACTOR = 0.6;
	private Node[] areaPrimaria;
	public SCHashTable(int pM)
	{	
		N = 0;
		M = pM;
		areaPrimaria = new Node[M];
	}
	
	public SCHashTable()
	{
		this(CAPACIDAD_INICIAL);
	}
	
	public int getM()
	{
		return M;
	}
	
	public int getN()
	{
		return N;
	}
	
	@Override
	public void put(K pK, V pV) {
		
		int i = hash(pK);
		 
		for (Node x = areaPrimaria[i]; x != null; x = x.next)
		{
			if (pK.equals(x.key)) 
			{ 
				x.value = pV; 
				return; 
			}
			 
		}
		areaPrimaria[i] = new Node(pK, pV, areaPrimaria[i]);
		N++;
		
		if(M > 0)
		{
			if(N/M > FACTOR)
			{
				rehash();
			}
		}
		
	}

	@Override
	public V get(K pK) {
		
		 int i = hash(pK);
		 
		 for (Node x = areaPrimaria[i]; x != null; x = x.next) 
		 {
			 if (pK.equals(x.key)) 
			 {
				return (V) x.value; 
			 }
		 }
		 return null;
	}

	@Override
	public V delete(K pK) {
		
		V buscado = get(pK);
		
		put(pK,null);
		
		return buscado;
	}

	@Override
	public Iterable<K> keys() {
		
		MiLista<K> lista = new MiLista<K>();
		
        for (int i = 0; i < M; i++) 
        {
        	Node actual = areaPrimaria[i];
        	
            while(actual != null)
            {
            	K k1 = (K) actual.key;
            	lista.agregarElemento(k1);
            	actual = actual.next;
            }
                
        }
        return lista; 
	}

	@Override
	public int hash(K pK) {
		return Math.abs((pK.hashCode() * 0x7fffffff) % M);
	}
	
	@Override
	public void rehash()
	{	
		SCHashTable<K,V> nueva = new SCHashTable<K,V>(nuevoM());
		MiLista<K> lista = (MiLista<K>) keys();
		
		for(int i = 0; i < lista.darTamanio(); i++)
		{
			K actual = lista.darElementoPosicion(i);
			nueva.put(actual, get(actual));
		}
		
		
		
		M = nueva.M;
		N = nueva.N;
		areaPrimaria = nueva.areaPrimaria;
	}
	
	//M�todos auxiliares
	//----------------------------------------------------------------------------------------------------
	
	/*
	 * Revisa si un n�mero es primo
	 * Tomado de: https://www.mkyong.com/java/how-to-determine-a-prime-number-in-java/
	 */
	public boolean esPrimo(int n) 
	{
	    //revisa si n es m�ltiplo de 2
	    if (n%2==0) return false;
	    //si no, revisa s�lo los impares
	    for(int i=3;i*i<=n;i+=2) {
	        if(n%i==0)
	            return false;
	    }
	    return true;
	}
	
	/*
	 * Agranda el tama�o del areglo al siguiente n�mero primo del doble del tama�o actual.
	 */
	public int nuevoM()
	{
		for(int i = M*2;; i++)
		{
			if(esPrimo(i))
			{	
				return i;
			}
		}
	}
	
	private static class Node{
		
		private Object key;
		private Object value;
		private Node next;
		
		public Node(Object pKey, Object pValue, Node pNext)
		{
			key = pKey;
			value = pValue;
			next = pNext;
		}
		
	}

}
