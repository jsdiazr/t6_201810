package model.data_structures;

public class LPHashTable<K,V> implements IHashTable<K,V>{

	public static final int CAPACIDAD_INICIAL = 13;
	public static final double FACTOR = 0.5;
	
	private int N;
	private int M;
	private K[] keys;
	private V[] vals;
	
	public LPHashTable(int pM)
	{
		M = pM;
		keys = (K[]) new Object[M];
		vals = (V[]) new Object[M];
	}
	
	public LPHashTable()
	{
		this(CAPACIDAD_INICIAL);
	}
	
	public int getM()
	{
		return M;
	}
	
	public int getN()
	{
		return N;
	}
	
	@Override
	public void put(K pK, V pV) 
	{
		if (N/M >= FACTOR)
		{
			rehash();
		}
		
		int i;
		for (i = hash(pK); keys[i] != null; i = (i + 1) % M)
		{
			if (keys[i].equals(pK))
			{
				vals[i] = (V) pV; return; 
			}
		}
		
		keys[i] = (K) pK;
		vals[i] = (V) pV;
		N++;
	}

	@Override
	public V get(K pK) 
	{ 
		for (int i = hash(pK); keys[i] != null; i = (i + 1) % M)
		{
			if (keys[i].equals(pK))
			{
				return vals[i];
			}
				
		}
		return null;
	}
	
	@Override
	public V delete(K pK) 
	{
		if (!contains(pK)) 
		{
			return null;
		}
		
		V buscado = get(pK);
		
		int i = hash(pK);
		
		while (!pK.equals(keys[i]))
		{
			System.out.println("1");
			i = (i + 1) % M;
		
		}
		
		keys[i] = null;
		vals[i] = null;
		i = (i + 1) % M;
		
		while (keys[i] != null)
		{
			System.out.println("2");
			K keyToRedo = keys[i];
			V valToRedo = vals[i];
			keys[i] = null;
			vals[i] = null;
			N--;
			put(keyToRedo, valToRedo);
			i = (i + 1) % M;
		}
		N--;
		
		return buscado;
	}

	public boolean contains(K pK) 
	{
		return get(pK) != null;
	}

	@Override
	public Iterable<K> keys() 
	{
		MiLista<K> lista = new MiLista<K>();
		
		for(int i = 0; i < keys.length; i++)
		{
			K actual = keys[i];
			
			if(actual != null)
			{
				lista.agregarElemento(actual);
			}
			
		}
		
		return lista;
	}

	@Override
	public int hash(K pK) {
		return Math.abs((pK.hashCode() * 0x7fffffff) % M);
	}

	@Override
	public void rehash() 
	{
		LPHashTable<K, V> t;
		t = new LPHashTable<K, V>(nuevoM());
		for (int i = 0; i < M; i++)
		{
			if (keys[i] != null)
			{
				t.put(keys[i], vals[i]);
			}
		}

		keys = t.keys;
		vals = t.vals;
		M = t.M;
		
	}

	//M�todos auxiliares
	//----------------------------------------------------------------------------------------------------
		
		/*
		 * Revisa si un n�mero es primo
		 * Tomado de: https://www.mkyong.com/java/how-to-determine-a-prime-number-in-java/
		 */
		public boolean esPrimo(int n) 
		{
		    //revisa si n es m�ltiplo de 2
		    if (n%2==0) return false;
		    //si no, revisa s�lo los impares
		    for(int i=3;i*i<=n;i+=2) {
		        if(n%i==0)
		            return false;
		    }
		    return true;
		}
		
		/*
		 * Agranda el tama�o del areglo al siguiente n�mero primo del doble del tama�o actual.
		 */
		public int nuevoM()
		{
			for(int i = M*2;; i++)
			{
				if(esPrimo(i))
				{	
					return i;
				}
			}
		}
	
}
