package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {
	
	private String trip_id;
	private String trip_seconds;
	private String trip_miles;
	private String pickup_community_area;
	private String trip_start_timestamp;
	private String trip_end_timestamp;
	
	public Service(String pTrip_id, String pTrip_seconds, String pTrip_miles, String pPickup_community_area,
					String pTrip_start_timestamp, String pTrip_end_timestamp)
	{
		trip_id = pTrip_id;
		trip_seconds = pTrip_seconds;
		trip_miles = pTrip_miles;
		pickup_community_area = pPickup_community_area;
		trip_start_timestamp = pTrip_start_timestamp;
		trip_end_timestamp = pTrip_end_timestamp;
	}
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		return trip_id;
	}	
		
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public String getTripSeconds() 
	{
		return trip_seconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public String getTripMiles() {
		return trip_miles;
	}

	
	public String getTripStartTimeStamp()
	{
		return trip_start_timestamp;
	}
	
	public String getTripEndTimeStamp()
	{
		return trip_end_timestamp;
	}
	
	public String getFechaI()
	{
		String startS[] = trip_start_timestamp.split("T");
		String f = startS[0];
		String rta[] = f.split("-");
		return rta[0] + rta[1] + rta[2];
	}
	
	public String getFechaF()
	{
		String startS[] = trip_end_timestamp.split("T");
		String f = startS[0];
		String rta[] = f.split("-");
		return rta[0] + rta[1] + rta[2];
	}
	
	public String getHoraI()
	{
		String startS[] = trip_start_timestamp.split("T");
		String t = startS[1];
		String a[] = t.split(":");
		return a[0] + a[1];
	}
	
	public String getHoraF()
	{
		String startS[] = trip_end_timestamp.split("T");
		String t = startS[1];
		String a[] = t.split(":");
		return a[0] + a[1];
	}

	@Override
	public int compareTo(Service o) {
		
		int f1 = Integer.parseInt(getFechaI());
		int f2 = Integer.parseInt(o.getFechaI());
		
		int h1 = Integer.parseInt(getHoraI());
		int h2 = Integer.parseInt(o.getHoraI());
		
		int rta;
		
		if(f1 == f2)
		{
			if(h1 == h2)
			{
				rta = 0;
			}
			else if(h1 < h2)
			{
				rta = -1;
			}
			else
			{
				rta = 1;
			}
		}
		else if(f1 < f2)
		{
			rta = -1;
		}
		else
		{
			rta = 1;
		}
		
		return rta;
		
	}
}
