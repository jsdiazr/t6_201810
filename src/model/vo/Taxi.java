package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	private String taxi_id;
	private String company;
	
	public Taxi(String pTaxi_id, String pCompany)
	{
		taxi_id = pTaxi_id;
		company = pCompany;
	}
	
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		return taxi_id;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		return company;
	}
	
	@Override
	public int compareTo(Taxi o) {
		// TODO Auto-generated method stub
		return 0;
	}	
}
