package view;

import java.util.Scanner;

import controller.Controller;
import model.vo.Taxi;
import model.vo.Service;

public class TaxiTripsManagerView 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option)
			{
				case 1:
					Controller.loadServices();
					break;
				case 2:	
					System.out.println("Ingrese el n�mero del �rea");
					String area = sc.next();
					Controller.serviciosEnArea(area);
				case 3:
					System.out.println("Ingrese la distancia recorrida");
					double distancia = Double.parseDouble(sc.next());
					Controller.agruparServiciosXDistancia(distancia);
				case 4:	
					fin=true;
					sc.close();
					break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 6----------------------");
		System.out.println("1. Cargar los servicios de un taxi reportados en un subconjunto de datos");
		System.out.println("2. Mostrar servicios que inician en el �rea dada en orden cronol�gico");
		System.out.println("3. Mostrar el conjunto de servicios con una distancia recorrida");
		System.out.println("4. Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
}
