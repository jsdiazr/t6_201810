package api;

import model.data_structures.MiLista;
import model.vo.Service;

/**
 * Basic API for testing the functionality of the TaxiTrip manager
 */
public interface ITaxiTripsManager {

	/**
	 * Method to load the services of a specific taxi 
	 * The services are loaded in both a Stack and a Queue 
	 * @param servicesFile - path to the JSON file with taxi services 
	 * @param taxiId - taxiId of interest 
	 */
	public void loadServices(String serviceFile);

	public MiLista<Service> serviciosEnArea(String pArea); 
	
	public MiLista<Service> agruparServiciosXDistancia(double pDistancia);
	
}
